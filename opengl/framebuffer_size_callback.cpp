

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <stdio.h>
#include <stdlib.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "globals.h"

#include "WASD.h"

void framebuffer_size_callback(GLFWwindow* window, int new_width, int new_height)
{
	width = new_width;
	height = new_height;
	glViewport(0, 0, width, height);
	opengl_controls_recalculate();
}
