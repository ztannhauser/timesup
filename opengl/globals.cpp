#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <stdio.h>
#include <stdlib.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

GLFWwindow *window;
int width, height;
glm::mat4 ViewMatrix;
glm::mat4 ProjectionMatrix;
float fov;
GLuint MPVID;
GLuint TranslationID;
GLuint ColorID;
GLuint cubebuffer;
GLuint linebuffer;

glm::vec3 position = glm::vec3(0, 0, 2);
