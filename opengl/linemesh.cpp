#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <stdio.h>
#include <stdlib.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#define R 0.51f

GLfloat linemesh[100] = {
	-R, +R, +R,
	+R, +R, +R,
	+R, +R, +R,
	+R, +R, -R,
	+R, +R, -R,
	-R, +R, -R,
	-R, +R, -R,
	-R, +R, +R,
	
	+R, +R, +R,
	+R, -R, +R,
	+R, +R, -R,
	+R, -R, -R,
	-R, +R, -R,
	-R, -R, -R,
	-R, +R, +R,
	-R, -R, +R,
	
	-R, -R, +R,
	+R, -R, +R,
	+R, -R, +R,
	+R, -R, -R,
	+R, -R, -R,
	-R, -R, -R,
	-R, -R, -R,
	-R, -R, +R
};
	
