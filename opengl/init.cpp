
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <stdio.h>
#include <stdlib.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "globals.h"
#include "framebuffer_size_callback.h"
#include "scroll_callback.h"
#include "fragmentsshader.h"
#include "vertexshader.h"
#include "cubemesh.h"
#include "linemesh.h"

int programID;
GLuint VertexArrayID;

int init()
{
	assert(glfwInit());
	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	assert(window = glfwCreateWindow(width =
		1024, height = 768, __FILE__, NULL, NULL));
	glfwMakeContextCurrent(window);
	fov = 70;
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
	glfwSetScrollCallback(window, scroll_callback);
	glewExperimental = true;
	assert(glewInit() == GLEW_OK);
	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
//	glClearColor(0.0f, 0.0f, 0.4f, 0.0f);
	glClearColor(1, 1, 1, 0.0f);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	glGenVertexArrays(1, &VertexArrayID);
	glBindVertexArray(VertexArrayID);
	{
		GLint Result = GL_FALSE;
		int InfoLogLength;
		GLuint VertexShaderID = glCreateShader(GL_VERTEX_SHADER);
		GLuint FragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);
		glShaderSource(VertexShaderID, 1, &vertexshader, NULL);
		glCompileShader(VertexShaderID);
		glGetShaderiv(VertexShaderID, GL_COMPILE_STATUS, &Result);
		glGetShaderiv(VertexShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
		assert(!(InfoLogLength > 0));
		glShaderSource(FragmentShaderID, 1, &fragmentsshader, NULL);
		glCompileShader(FragmentShaderID);
		glGetShaderiv(FragmentShaderID, GL_COMPILE_STATUS, &Result);
		glGetShaderiv(FragmentShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
		assert(!(InfoLogLength > 0));
		programID = glCreateProgram();
		glAttachShader(programID, VertexShaderID);
		glAttachShader(programID, FragmentShaderID);
		glLinkProgram(programID);
		glGetProgramiv(programID, GL_LINK_STATUS, &Result);
		glGetProgramiv(programID, GL_INFO_LOG_LENGTH, &InfoLogLength);
		assert(!(InfoLogLength > 0));
		glDetachShader(programID, VertexShaderID);
		glDetachShader(programID, FragmentShaderID);
		glDeleteShader(VertexShaderID);
		glDeleteShader(FragmentShaderID);
	}
	
	MPVID = glGetUniformLocation(programID, "MVP");
	TranslationID = glGetUniformLocation(programID, "translation");
	ColorID = glGetUniformLocation(programID, "color");
	glGenBuffers(1, &cubebuffer);
	glBindBuffer(GL_ARRAY_BUFFER, cubebuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cubemesh), cubemesh, GL_STATIC_DRAW);
	glGenBuffers(1, &linebuffer);
	glBindBuffer(GL_ARRAY_BUFFER, linebuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(linemesh), linemesh, GL_STATIC_DRAW);
	glUseProgram(programID);
	glEnableVertexAttribArray(0);
}

int uninit()
{
	glDisableVertexAttribArray(0);
	glDeleteBuffers(1, &cubebuffer);
	glDeleteBuffers(1, &linebuffer);
	glDeleteProgram(programID);
	glDeleteVertexArrays(1, &VertexArrayID);
	glfwTerminate();
}
