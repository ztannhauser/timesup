#include <stdio.h>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <stdio.h>
#include <stdlib.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "globals.h"

glm::vec3 up = glm::vec3(0, 1, 0);

float horizontalAngle = 3.14f;
float verticalAngle = 0.0f;
float speed = 10.0f;
float Yspeed = 30.0f;
float mouseSpeed = 0.002f;

void opengl_controls_recalculate()
{
	//printf("width == %i\n", width);
	//printf("height == %i\n", height);
	glm::vec3 direction(cos(verticalAngle) * sin(horizontalAngle),
						sin(verticalAngle),
						cos(verticalAngle) * cos(horizontalAngle));
	ProjectionMatrix = glm::perspective(
		glm::radians(fov), float(width) / float(height), 0.1f, 1000.0f);
	ViewMatrix = glm::lookAt(position, position + direction, up);
}

void opengl_controls_WASD() {
	static double lastTime = glfwGetTime();

	double currentTime = glfwGetTime();
	float deltaTime = float(currentTime - lastTime);

	double xpos, ypos;
	glfwGetCursorPos(window, &xpos, &ypos);

	glfwSetCursorPos(window, width / 2, height / 2);

	horizontalAngle += mouseSpeed * float(width / 2 - xpos);
	verticalAngle += mouseSpeed * float(height / 2 - ypos);

	glm::vec3 direction(sin(horizontalAngle), 0, cos(horizontalAngle));
	glm::vec3 right = glm::vec3(sin(horizontalAngle - 3.14f / 2.0f), 0,
								cos(horizontalAngle - 3.14f / 2.0f));
	if(glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
		position += direction * deltaTime * speed;
	}
	if(glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
		position -= direction * deltaTime * speed;
	}
	if(glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {
		position += right * deltaTime * speed;
	}
	if(glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
		position -= right * deltaTime * speed;
	}
	if(glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS) {
		position += up * deltaTime * Yspeed;
	}
	if(glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS) {
		position -= up * deltaTime * Yspeed;
	}
	opengl_controls_recalculate();
	lastTime = currentTime;
}
