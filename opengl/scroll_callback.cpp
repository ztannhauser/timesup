
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <stdio.h>
#include <stdlib.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "globals.h"
#include "WASD.h"

void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
	if(yoffset < 0)
		fov *= 100.0 / 99.0;
	else
		fov *= 99.0 / 100.0;
	opengl_controls_recalculate();
}
