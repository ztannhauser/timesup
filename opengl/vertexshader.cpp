const char* vertexshader = 
	"#version 330 core\n"
	"layout(location = 0) in vec3 vertexPosition_modelspace;\n"
	"out vec3 fragmentColor;\n"
	"uniform mat4 MVP;\n"
	"uniform vec3 translation;\n"
	"uniform vec3 color;\n"
	"void main(){\n"
	"	vec3 temp = vertexPosition_modelspace;\n"
	"	temp += translation;\n"
	"	gl_Position =  MVP * vec4(temp, 1);\n"
	"	fragmentColor = color;\n"
	"}\n";
