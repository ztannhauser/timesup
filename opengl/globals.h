
extern GLFWwindow *window;
extern int width, height;


extern glm::mat4 ViewMatrix;
extern glm::mat4 ProjectionMatrix;

extern float fov;  // initialise me!

extern GLuint MPVID;
extern GLuint TranslationID;
extern GLuint ColorID;
extern GLuint cubebuffer;
extern GLuint linebuffer;
extern glm::vec3 position;
