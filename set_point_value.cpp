extern "C" {
#define new something_else
#include <avl.h>
#undef new
}
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "point2d.h"

void set_point_value(avl_tree_t* tree, int x, int z) {
	struct point2d temp = {.x = x, .z = z};
	avl_node_t* result = avl_search(tree, &temp);
	assert(!result);
	struct point2d* temp2 = (struct point2d*)
		memcpy(malloc(sizeof(struct point2d)), &temp, sizeof(struct point2d));
	avl_insert(tree, temp2);
}
