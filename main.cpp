extern "C" {
#define new something_else
#include <avl.h>
#undef new
}
#include <stdio.h>
#include <stdbool.h>
#include <GL/glew.h>
#include <assert.h>
#include <string.h>

#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "opengl/WASD.h"
#include "opengl/globals.h"
#include "opengl/init.h"

#include "point2d.h"
#include "point3d.h"
#include "set_point_value.h"
#include "compare2d.h"
#include "compare3d.h"

glm::vec3 yellow = glm::vec3(1, 1, 0);
glm::vec3 black = glm::vec3(0, 0, 0);
glm::vec3 red = glm::vec3(1, 0, 0);
glm::vec3 blue = glm::vec3(0, 0, 1);

void draw_at(glm::vec3 translation, bool active)
{
//	translation.y *= 1.1;
	glUniform3fv(TranslationID, 1, &(translation[0]));

	glBindBuffer(GL_ARRAY_BUFFER, cubebuffer);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void *) 0);
	glUniform3fv(ColorID, 1, &(red[0]));
	glDrawArrays(GL_TRIANGLES, 0, 12 * 3);

	glBindBuffer(GL_ARRAY_BUFFER, linebuffer);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void *) 0);
	glUniform3fv(ColorID, 1, active ? &(yellow[0]) : &(black[0]));
	glDrawArrays(GL_LINES, 0, 24 * 3);
}

int level = 0;

void transfer(avl_tree_t* current, avl_tree_t* all)
{
	for(avl_node_t* node = current->head; node; node = (node->next))
	{
		struct point2d p = *((struct point2d*) node->item);
		{
			struct point3d* n = (struct point3d*) malloc(sizeof(struct point3d));
			n->x = p.x;
			n->y = level;
//			n->y = 0;
			n->z = p.z;
			avl_insert(all, n);
		}
	}
}

struct {signed int x, z;} nearby[] = {
	{+1, +1}, {+1, +0}, {+1, -1},
	{+0, +1},           {+0, -1},
	{-1, +1}, {-1, +0}, {-1, -1},
};

avl_tree_t* tick(avl_tree_t* last)
{
	avl_tree_t* current = avl_alloc_tree((avl_compare_t) compare2d, free);
	for(avl_node_t* node = last->head; node; node = (node->next))
	{
		struct point2d p = *((struct point2d*) node->item);
		{
			for(int i = 0;i < 8;i++)
			{
				struct point2d t = {
					.x = p.x + nearby[i].x,
					.z = p.z + nearby[i].z
				};
				if(!avl_search(current, &t))
				{
					int count = 0;
					for(int j = 0;j < 8;j++)
					{
						struct point2d s = {
							.x = t.x + nearby[j].x,
							.z = t.z + nearby[j].z
						};
						count += avl_search(last, &s) ? 1 : 0;
					}
					if((count == 3) // || (count == 4)
						|| (avl_search(last, &t) && (count == 2)))
					{
				avl_insert(current, memcpy(malloc(sizeof(t)), &t, sizeof(t)));
					}
				}
			}
		}
	}
	avl_free_tree(last);
	return current;
}

int main() {
	init();

	avl_tree_t* all = avl_alloc_tree((avl_compare_t) compare3d, free);
	avl_tree_t* current = avl_alloc_tree((avl_compare_t) compare2d, free);
	avl_node_t *node;
	
	set_point_value(current, -2, +0);
	set_point_value(current, -1, +0);
	set_point_value(current, -0, +0);
	set_point_value(current, +1, +0);
	set_point_value(current, +2, +0);
	
	set_point_value(current, +0, +1);
	set_point_value(current, +2, +1);
	
	set_point_value(current, -2, +2);
	set_point_value(current, +0, +2);
	set_point_value(current, +2, +2);
	
	set_point_value(current, -2, +3);
	set_point_value(current, -1, +3);


	while(glfwGetKey(window, GLFW_KEY_ESCAPE) != GLFW_PRESS &&
		  glfwWindowShouldClose(window) == 0) {
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		opengl_controls_WASD();
		glm::mat4 MVP = ProjectionMatrix * ViewMatrix;
		glUniformMatrix4fv(MPVID, 1, GL_FALSE, &MVP[0][0]);
		if(glfwGetKey(window, GLFW_KEY_Q) == GLFW_PRESS)
		{
			transfer(current, all);
			current = tick(current);
//			level--;
			level++;
			position.y++;
			
//		}
//		if(glfwGetKey(window, GLFW_KEY_R) == GLFW_PRESS)
//		{
		
		
//			for(avl_node_t* node = all->head; node; node = (node->next))
//			{
//				struct point3d p = *((struct point3d*) node->item);
//				if(hypot(hypot(p.x - position.x, p.y - position.y),
//					p.z - position.z) > 10)
//				{
//					avl_delete_node(all, node);
//					node = all->head;
//					if(!node)
//					{
//						break;
//					}
//				}
//			}

//			avl_free_nodes(all);

			for(avl_node_t* node = all->head; node; node = (node->next))
			{
				struct point3d p = *((struct point3d*) node->item);
				if(p.y < int(position.y) - 100)
				{
					avl_delete_node(all, node);
					node = all->head;
					if(!node)
					{
						break;
					}
				}
			}

		}
		for(avl_node_t* node = all->head; node; node = (node->next))
		{
			struct point3d p = *((struct point3d*) node->item);
			draw_at(glm::vec3(p.x, p.y, p.z), false);
		}
		for(avl_node_t* node = current->head; node; node = (node->next))
		{
			struct point2d p = *((struct point2d*) node->item);
			draw_at(glm::vec3(p.x, level, p.z), true);
		}
//		draw_at(glm::vec3(0, 1, 0), true);
		glfwSwapBuffers(window);
		glfwPollEvents();
	}
	avl_free_tree(all);
	avl_free_tree(current);
	uninit();
	return 0;
}

